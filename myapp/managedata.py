'''
fichier qui contient les fonctions relatives à la gestion des données
'''
import pandas as pd

def read_data_file(file):
    '''
    fonction qui lit le fichier de donnée et qui supprime les colonnes 'index' et 'Patient Id'
    '''
    df = pd.read_csv(file)
    df.drop(columns=['index', 'Patient Id'], axis=1, inplace=True)
    return df
