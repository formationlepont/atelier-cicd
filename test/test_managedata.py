'''
fichier de test des fonction de gestion des données
'''

#import pytest
#import pandas as pd
from myapp.managedata import read_data_file

def test_read_data():
    '''
    test de la lecture des données
    '''
    df = read_data_file("data/cancer patient data sets.csv")
    assert df.shape == (1000,24), "Le dataframe devrait etre de 1000 lignes sur 24 colonnes"
